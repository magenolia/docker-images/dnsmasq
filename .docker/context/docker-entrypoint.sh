#!/usr/bin/env sh
set -o errexit -o nounset -o noclobber

# Configure dnsmasq domains and addresses
for dnsmasq_dns_entry in $(echo "${DNSMASQ_DNS_MAP}" | tr "," "\n")
do
  dnsmasq_domain="$(echo "${dnsmasq_dns_entry}" | cut -d: -f1 )"
  dnsmasq_address="$(echo "${dnsmasq_dns_entry}" | cut -d: -f2 )"

  if [ "${dnsmasq_address}" = "${dnsmasq_dns_entry}" ]; then
    dnsmasq_address='127.0.0.1'
  fi

  echo "address=/${dnsmasq_domain}/${dnsmasq_address}" >> /etc/dnsmasq.conf
  echo "[+] Configured domain ${dnsmasq_domain} with address ${dnsmasq_address}."
done

exec "${@-}"
