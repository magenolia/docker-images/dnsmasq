<p align="center">
    <img src="https://gitlab.com/magenolia/assets/-/raw/main/magenolia-logo.png" alt="Magenolia" width="250"/>
    <img src="https://gitlab.com/magenolia/assets/-/raw/main/dnsmasq-logo.png" alt="dnsmasq" width="250"/>
</p>

# dnsmasq

This repository is the source of a general purpose **dnsmasq** Docker image.

It allows **forcing DNS resolution for a given domain (and subdomains) to any specific ip**.

This is mainly useful for **pointing local development domains to current host without messing with `/etc/hosts`**, by adding / removing individual entries.

For instance, you can configure once `dev.lo` domain to resolve to localhost, and have automatically any of its subdomains pointing also there.

Click below to see it in action (you can copy text directly from the player):

[![asciicast](https://asciinema.org/a/463772.svg)](https://asciinema.org/a/463772?autoplay=true)

# Table of contents

[[_TOC_]]

# Supported platforms

- linux/amd64
- linux/arm64/v8

# Tags

This image is based on `alpine:3`, and is versionless. Only `latest` tag will be provided.

# Prerequisites

- Ensure you have **`docker` installed and available** in the command line.
- Before proceeding, **ensure port 53 is available** in your computer, as it is required for dnsmasq to work.

You can check if port is available with this command:

```shell
! lsof -i udp:53 >/dev/null 2>&1 || { >&2 echo "Port 53 is not available." && return 1 }
```

> You may need to stop and uninstall dnsmasq, in Mac:
>
> ```shell
> sudo brew services stop dnsmasq
> brew remove dnsmasq;

# Quick install

Run installer executing command below:

```shell
/bin/sh -c "$(curl -fsSL https://gitlab.com/magenolia/docker-images-group/dnsmasq/-/raw/latest/bin/install)"
```

Command above will point `local`, `dev.local`, `lo`, `dev.lo` and `test` domains to `127.0.0.1`.

## Quick install customization

Define `DNSMASQ_DNS_MAP` variable with comma separated values, each of one in format: DOMAIN[:ADDRESS],
where ADDRESS is optional and defaults to `127.0.0.1`. For example, you could run the following:

```shell
DNSMASQ_DNS_MAP=google:8.8.8.8,dev.lo:127.0.0.1 /bin/sh -c "$(curl -fsSL https://gitlab.com/magenolia/docker-images-group/dnsmasq/-/raw/latest/bin/install)"
```

to obtain this result:

```shell
> ping -c 1 testing.google && ping -c 1 testing.dev.lo
PING testing.google (8.8.8.8): 56 data bytes
64 bytes from 8.8.8.8: icmp_seq=0 ttl=119 time=20.091 ms

--- testing.google ping statistics ---
1 packets transmitted, 1 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 20.091/20.091/20.091/0.000 ms
PING testing.dev.lo (127.0.0.1): 56 data bytes
64 bytes from 127.0.0.1: icmp_seq=0 ttl=64 time=0.050 ms

--- testing.dev.lo ping statistics ---
1 packets transmitted, 1 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 0.050/0.050/0.050/0.000 ms
```

# Quick uninstall

Run uninstaller executing command below:

```shell
/bin/sh -c "$(curl -fsSL https://gitlab.com/magenolia/docker-images-group/dnsmasq/-/raw/latest/bin/uninstall)"
```

# Change domains and / or addresses

If you change your mind about the domains or addresses to be used, you can achieve it **simply running installer again with the updated `DNSMASQ_DNS_MAP` specification**, following the steps described above.

# Advanced configuration

If you need more advanced customization, **use a volume to mount your custom dnsmasq configuration file** to `/etc/dnsmasq.conf`.

# Docker compose

Take a look at [docker-compose.yml](https://gitlab.com/magenolia/docker-images-group/dnsmasq/-/blob/latest/docker-compose.yml) and [.env](https://gitlab.com/magenolia/docker-images-group/dnsmasq/-/blob/latest/.env) files for further reference.
